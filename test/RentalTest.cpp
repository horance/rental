#include <gtest/gtest.h>
#include "rental/Rental.h"
#include "rental/Customer.h"

struct RentalTest : ::testing::Test
{
    RentalTest& given_customer()
    {
        return *this;
    }

    const RentalTest& when_rent(std::initializer_list<Rental> rentals)
    {
        for(auto &rental : rentals)
        {
            customer.addRental(rental);
        }

        return *this;
    }

    void then_total_charge_should_equal_to(const U32 totalCharge) const
    {
        ASSERT_EQ(totalCharge, customer.getTotalCharge());
    }

private:
    Customer customer;
};

TEST_F(RentalTest, should_get_total_charge_for_regular_movie)
{
    given_customer()
      .when_rent({Rental(REGULAR, 2)})
      .then_total_charge_should_equal_to(2);
}

TEST_F(RentalTest, should_get_total_charge_for_new_release_movie)
{
    given_customer()
      .when_rent({Rental(NEW_RELEASE, 3)})
      .then_total_charge_should_equal_to(9);
}

TEST_F(RentalTest, should_get_total_charge_for_childrens_movie)
{
    given_customer()
      .when_rent({Rental(CHILDRENS, 4)})
      .then_total_charge_should_equal_to(4);
}

TEST_F(RentalTest, should_get_total_charge)
{
    given_customer()
      .when_rent({Rental(REGULAR, 2), Rental(NEW_RELEASE, 3), Rental(CHILDRENS, 4)})
      .then_total_charge_should_equal_to(15);
}
