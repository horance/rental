#include "rental/Customer.h"

void Customer::addRental(const Rental& rental)
{
    rentals.emplace_back(rental);
}

U32 Customer::getTotalCharge() const
{
    auto totalCharge = 0;

    for(auto& rental : rentals)
    {
        totalCharge += rental.getCharge();
    }
    
    return totalCharge; 
}

