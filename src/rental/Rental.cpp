#include "rental/Rental.h"
#include <functional>

Rental::Rental(const MovieType priceCode, const U8 daysRented)
  : movieType(priceCode), daysRented(daysRented)
{}

namespace
{
    auto regular = [](const U8 daysRented)
    {
        auto charge = 2;

        if (daysRented > 2)
        {
            charge += (daysRented - 2) * 2;
        }

        return charge;
    };

    auto new_release = [](const U8 daysRented)
    {
        return daysRented * 3;
    };

    auto childrens = [](const U8 daysRented)
    {
        auto charge = 1;

        if (daysRented > 3)
        {
            charge += (daysRented - 3) * 3;
        }

        return charge;
    };

    const std::function<U32(const U8)> prices[] =
    {
         regular,
         new_release,
         childrens,
    };
}

U32 Rental::getCharge() const
{
    return prices[movieType](daysRented);
}
