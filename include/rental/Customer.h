#ifndef INLWEOI45_57365_FYEUUTEWQOT_NBMEJEWO2HT_47563HQYW
#define INLWEOI45_57365_FYEUUTEWQOT_NBMEJEWO2HT_47563HQYW

#include <vector>
#include "rental/Rental.h"

struct Customer
{
    void addRental(const Rental& rental);
    U32 getTotalCharge() const;

private:
    std::vector<Rental> rentals;
};

#endif

