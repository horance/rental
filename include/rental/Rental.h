#ifndef VILASDFE875_LERWUOI97463_MKGLIJELK83274673
#define VILASDFE875_LERWUOI97463_MKGLIJELK83274673

#include "base/BaseTypes.h"

enum MovieType
{
    REGULAR,
    NEW_RELEASE,
    CHILDRENS,
};

struct Rental
{
     Rental(const MovieType priceCode, const U8 daysRented);

     U32 getCharge() const;

private:
     const MovieType movieType;
     const U8 daysRented;
};

#endif
