#ifndef INLGWPGKWEOIT_58395JFLSG_RUTOOJVN_HRYOQOJNO6847201
#define INLGWPGKWEOIT_58395JFLSG_RUTOOJVN_HRYOQOJNO6847201

#define ABSTRACT(...) virtual __VA_ARGS__ = 0
#define OVERRIDE(...) virtual __VA_ARGS__
#define EXTENDS(...) , ##__VA_ARGS__
#define IMPLEMENTS(...) EXTENDS(__VA_ARGS__)

namespace details
{
    template <typename T>
    struct Role
    {
        virtual ~Role() {}
    };
}

#define DEFINE_ROLE(type) struct type : ::details::Role<type>

#endif
