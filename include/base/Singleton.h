#ifndef RUOQWU_475610_VNLALKZLKSFOWIEROITQP_579230457_FJQWOIR
#define RUOQWU_475610_VNLALKZLKSFOWIEROITQP_579230457_FJQWOIR

template <typename T>
struct Singleton
{
    static T& getInstance()
    {
        static T inst;
        return inst;
    }

protected:
    Singleton() {}

private:
    Singleton(const Singleton&);
    Singleton& operator=(const Singleton&);
};

#define DEFINE_SINGLETON(type) struct type : Singleton<type>

#endif

